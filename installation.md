## Install terraform

reference: https://www.youtube.com/watch?v=SLB_c_ayRMo 

- MacOS:

run this command in terminal. If you do not have brew, than install homebrew first.
```
brew install terraform
```

after you run this command, make sure that install properly

```
terraform -v
```

This will show you the version correctly if it installed correctly


- Windows:
1) Google "terraform" and go to the terraform.io home page

2) download terraform for windows

3) unzip the file

4) There will be just one file in the unzipped folder. copy that file to a directory of your choice

5) Note down the exact path to this folder

5) search for "env" on the windows logo in the task bar

6) add a new path in the system variables and paste/type this path to the list

Now goto the cmd window and type

```
terraform -v
```

if the installation was successful, you will see a version number printed 


## Visual Studio Code

The preferred IDE of choice is visual studio code. If you do not have it, than download and install it. Otherwise, you can always use an editor of your choice.

Once in VS Code, add a terraform extension to the editor. It will help you with syntax and other features.

```
NOTE: Make sure to add an extension from HashiCorp 
```