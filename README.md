# docs

All documentations and notes to work with terraform

## Getting started

All my documentation and notes will be stored here for future references.
This repo group will deal with all experiments and projects. To initialize this repo, I have created two sepaarte projects
1) CreateEnv: Which will create AWS environment everyday to start the day
2) removeEnv: This will remove all environment at the end of the day

This experiment will help me learn how to use terraform and will also help me minimize cost on AWS. Why to deploy resources when we are not using them.

I will keep on updating this document as necessary