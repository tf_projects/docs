## Common commands and their usage

reference: https://registry.terraform.io/browse/providers

terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.0"
    }
  }
}

# Configure the AWS Provider
provider "aws" {
  region = "us-east-1"
}



Create the environment 

```
terraform init
```

See what will happen once this terraform will execute
```
terraform plan
``` 

Now run the code to create the resources needed
```
terraform apply
```

This will run the plan and show that information and then it will go through the environment and deploy all resources.
Terraform is "decorative" environment. We are showing how the end result should look like, so if we run it again and again, it will NOT keep on deploying these instances. Rather it will keep the environment with one instance



If you need to 'Destroy' this environment than you can run the following command
```
terraform destroy
``

It will remove all environmental changes made to the account

```
--auto-approve
```

Option above will not ask for a 'yes' while you run terraform apply or destroy

```
terraform state list
terraform state show <name>
```

```
terraform output

---- In the terraform code file
output "name"{
    value = <<resource name>>
}
```