## Variables in terraform
In order to parameterize the environment, we have variables in Terraform

reference: https://www.terraform.io/language/values/variables

reference: https://www.youtube.com/watch?v=3Yr-lHhBQm4

## Define a variable
In order to define a variable in terraform, see example below

```
variable "---variable name---"{}
```

This sample above is the simplest way of defining a variable. It is open ended and there is nothing defined other than the name of the variable. {variable name} will be unique to the environment and when you apply this terraform file, it will ask user for a value to be assigned to this variable.

## Use this variable

We can use this variable in the code like **var.{name of the variable}**

```
resource "aws.instance" "myEC2" {
    ami = "ami-123"
    instance_type = var.my_instance_type
    tags = var.my_EC2Tags
}
```

## Customize these variables
As a common practice, we will supply values while running apply to the terraform. These are some sample applications 

```
terraform apply -var="image_id=ami-abc123"
terraform apply -var='image_id_list=["ami-abc123","ami-def456"]' -var="instance_type=t2.micro"
terraform apply -var='image_id_map={"us-east-1":"ami-abc123","us-east-2":"ami-def456"}'
```

## Variables file

Rather than defining all your variables in a single terraform file, a common practice is to create a separate file which will have all the definitions of all the variables needed for the environment. A file name **variables.tf** will hold all the variables definitions 

```
variables.tf
```

In order to setup values of these variables at run time, we can supply a separate file. This variables file is named as 

```
terraform.tfvars
```
This file above will be loaded automatically. 

If you want to auto load a different file name than add **.auto.tfvars**. A Sample file name myvariables should be named like

```
myvariables.auto.tfvars
```

We can have different profiles for different environments to run against same infrastructure. Lets say we have a production environment and a development environment. We need a t2.micro for the development but we need a t2.large for a production environment. We create to separate files for these two separate environments. Our structure will look like :

##### variables.tf
```
variable EC2instance {
    type = string
    description = "Type of instance to initiate"
    default = "t2.micro"
}
```

#### Two separate profile files
Production File: **prod.tfvars**
```
EC2instance = "t2.Large"
```
Development File: **dev.tfvars**
```
EC2instance = "t2.Micro"
```

#### Which file to apply 
In order to run a profile we will do the following

```
terraform apply -var-file="prod.tfvars"

```


#### - Variable arguments

**default** - A default value which then makes the variable optional.

**type** - This argument specifies what value types are accepted for the variable.

**description** - This specifies the input variable's documentation.

**validation** - A block to define validation rules, usually in addition to type constraints.

**sensitive** - Limits Terraform UI output when the variable is used in configuration.

**nullable** - Specify if the variable can be null within the module.


#### - Variable types
Type constraints are created from a mixture of type keywords and type constructors. The supported type keywords are:

**string**

**number**

**bool**

The type constructors allow you to specify complex types such as collections:

**list(<TYPE>)**

**set(<TYPE>)**

**map(<TYPE>)**

**object({<ATTR NAME> = <TYPE>, ... })**

**tuple([<TYPE>, ...])**


#### Environment Variables
There are environment variables which can be defined and used with terraform. **TF_VAR_** followed by the name of a variable will define an environment variable to be used in terraform.

```
export TF_VAR_image_id=ami-abc123
terraform apply
```

#### Variables sequence

- The terraform.tfvars file, if present.
- The terraform.tfvars.json file, if present.
- Any *.auto.tfvars or *.auto.tfvars.json files, processed in lexical order of their filenames.
- Any -var and -var-file options on the command line, in the order they are provided. (This includes variables set by a Terraform Cloud workspace.)