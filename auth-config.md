## Authentication and Configuration

reference: https://registry.terraform.io/providers/hashicorp/aws/latest/docs

Configuration for the AWS Provider can be derived from several sources, which are applied in the following order:

- Parameters in the provider configuration
- Environment variables
- Shared credentials files
- Shared configuration files
- Container credentials
- Instance profile credentials and region

## Parameters
```
provider "aws" {
  region     = "us-west-2"
  access_key = "my-access-key"
  secret_key = "my-secret-key"
}
```
Access key and secret access key are hard coded in the file. This is not a good exercise because we are exposing this information to the world and anyone with this access can do damage to the account.

## Environment Variables

There are certain environment variables provided to the user
- AWS_ACCESS_KEY_ID="anaccesskey"
- AWS_SECRET_ACCESS_KEY="asecretkey"
- AWS_REGION="us-west-2"
- AWS_PROFILE
- AWS_CONFIG_FILE
- AWS_SHARED_CREDENTIALS_FILE

These variables can be set before triggering terraform and it will provide related information to terraform
Define a provider in terraform file

```
provider "aws" {}
```

Now you can provide environment vaiables access key and secret access key before running terraform.
You access information will be provided to the session and will not be a part of the code to be deployed. Hence it is not shared with anyone

```
$ export AWS_ACCESS_KEY_ID="anaccesskey"
$ export AWS_SECRET_ACCESS_KEY="asecretkey"
$ export AWS_REGION="us-west-2"
$ terraform plan
```

## Shared credentials files and Shared configuration files

The AWS Provider can source credentials and other settings from the shared configuration and credentials files. By default, these files are located at $HOME/.aws/config and $HOME/.aws/credentials on Linux and macOS, and "%USERPROFILE%\.aws\config" and "%USERPROFILE%\.aws\credentials" on Windows.

If no named profile is specified, the default profile is used. Use the profile parameter or AWS_PROFILE environment variable to specify a named profile.

The locations of the shared configuration and credentials files can be configured using either the parameters shared_config_files and shared_credentials_files or the environment variables AWS_CONFIG_FILE and AWS_SHARED_CREDENTIALS_FILE.

```
provider "aws" {
  shared_config_files      = ["/Users/tf_user/.aws/conf"]
  shared_credentials_files = ["/Users/tf_user/.aws/creds"]
  profile                  = "customprofile"
}
```

## Container credentials

In case we are using container services like ECS or CodeBuild and have IAM task roles configured, then AWS_CONTAINER_CREDENTIALS_RELATIVE_URI and AWS_CONTAINER_CREDENTIALS_FULL_URI are set for the user as environment variables. These are set automatically and can be updated manually.

For services like EKS, we have AWS_ROLE_ARN and AWS_WEB_IDENTITY_TOKEN_FILE

## Instance profile credentials and region

When the AWS Provider is running on an EC2 instance with an IAM Instance Profile set, the provider can source credentials from the EC2 Instance Metadata Service. Both IMDS v1 and IMDS v2 are supported.

A custom endpoint for the metadata service can be provided using the ec2_metadata_service_endpoint parameter or the AWS_EC2_METADATA_SERVICE_ENDPOINT environment variable.

##### - Assuming an IAM Role
If provided with a role ARN, the AWS Provider will attempt to assume this role using the supplied credentials.

```
provider "aws" {
  assume_role {
    role_arn     = "arn:aws:iam::123456789012:role/ROLE_NAME"
    session_name = "SESSION_NAME"
    external_id  = "EXTERNAL_ID"
  }
}
```

##### - Assuming an IAM Role Using A Web Identity

```
provider "aws" {
  assume_role {
    role_arn                = "arn:aws:iam::123456789012:role/ROLE_NAME"
    session_name            = "SESSION_NAME"
    web_identity_token_file = "/Users/tf_user/secrets/web-identity-token"
  }
}
```

## Using an External Credentials Process

```
provider "aws" {
  profile = "customprofile"
}
```

where the profile file will look like 

```
[profile customprofile]
credential_process = custom-process --username jdoe
```

